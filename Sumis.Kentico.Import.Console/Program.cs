﻿using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;

namespace Sumis.Kentico.Import.Console
{
	class Program
	{
		static void Main(string[] args)
		{
			XmlConfigurator.Configure();

			string basePath = @"C:\Projects\Sumis\Sumis Enterprise - Git\Sumis.SumisEnterprise.Kentico.Import\Packages";

			var importManager = new ImportManagerBuilder()
				.WithRunAlwaysPackages(Path.Combine(basePath, "ImportType.RunAlways"))
				.WithRunOnlyNewPackages(Path.Combine(basePath, "ImportType.RunOnlyIfNew"))

				.Build();

			IEnumerable<ImportResult> results = importManager.Import();
			foreach (var result in results)
			{
				System.Console.WriteLine("Package: {0}", Path.GetFileName(result.ImportPackageFilePath));
				System.Console.WriteLine("Status: {0}", result.Status);
				System.Console.WriteLine("Messages: {0}", string.Join(Environment.NewLine, result.Messages));
				System.Console.WriteLine("Warnings: {0}", string.Join(Environment.NewLine, result.Warnings));
				System.Console.WriteLine("Errors: {0}", string.Join(Environment.NewLine, result.Errors));
				System.Console.WriteLine();
			}

#if DEBUG
			System.Console.WriteLine("Press any key to close");
			System.Console.ReadKey();
#endif
		}
	}
}
