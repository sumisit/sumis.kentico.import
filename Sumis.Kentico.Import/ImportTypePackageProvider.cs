﻿using CMS.CMSImportExport;
using Sumis.Kentico.Import.PackageFiles;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using CMS.SiteProvider;

namespace Sumis.Kentico.Import
{
	public class ImportTypePackageProvider : IImportPackageProvider
	{
		private readonly IPackageFileProvider fileProvider;

		private ImportTypeEnum importType;

		public ImportTypePackageProvider(IPackageFileProvider fileProvider, ImportTypeEnum importType)
		{
			if (fileProvider == null)
				throw new ArgumentNullException("fileProvider");

			if (importType == null)
				throw new ArgumentNullException("importType");

			this.fileProvider = fileProvider;
			this.importType = importType;
		}

		public IEnumerable<SiteImportSettings> GetImportPackages()
		{
			var packageFiles = fileProvider.GetPackageFiles();

			List<SiteImportSettings> packages = new List<SiteImportSettings>();

			foreach (var packageFile in packageFiles)
			{
				var settings = GetSiteImportSettings(packageFile.FilePath);

				settings.SetSettings(ImportExportHelper.SETTINGS_UPDATE_SITE_DEFINITION, false);

				settings.SetSettings(ImportExportHelper.SETTINGS_DELETE_SITE, false);
				settings.SetSettings(ImportExportHelper.SETTINGS_ADD_SITE_BINDINGS, false);
				settings.SetSettings(ImportExportHelper.SETTINGS_RUN_SITE, false);
				settings.SetSettings(ImportExportHelper.SETTINGS_TASKS, false);

				settings.SetSettings(ImportExportHelper.SETTINGS_GLOBAL_FOLDERS, false);
				settings.SetSettings(ImportExportHelper.SETTINGS_ASSEMBLIES, false);
				settings.SetSettings(ImportExportHelper.SETTINGS_SITE_FOLDERS, false);

				if (packageFile.Options != null)
				{
					if (packageFile.Options.SiteCodes.Any())
					{
						foreach (var siteCode in packageFile.Options.SiteCodes)
						{
							var siteInfo = SiteInfoProvider.GetSiteInfo(siteCode);
							settings.SiteName = siteInfo.SiteName;
							settings.SiteId = siteInfo.SiteID;

							settings.ExistingSite = true;

							settings.SetSettings(ImportExportHelper.SETTINGS_OVERWRITE_SYSTEM_QUERIES, false);
							settings.SetSettings(ImportExportHelper.SETTINGS_SKIP_OBJECT_ON_TRANSLATION_ERROR, false);
							settings.SetSettings(ImportExportHelper.SETTINGS_TASKS, false);

							settings.SetSettings(ImportExportHelper.SETTINGS_GLOBAL_FOLDERS, false);
							settings.SetSettings(ImportExportHelper.SETTINGS_ASSEMBLIES, false);
							settings.SetSettings(ImportExportHelper.SETTINGS_SITE_FOLDERS, false);

							settings.LogSynchronization = false;
							settings.LogIntegration = false;


							settings.CopyFiles = false;


							packages.Add(settings);
						}
					}
				}
				packages.Add(settings);

			}

			return packages;
		}

		private SiteImportSettings GetSiteImportSettings(string sourceFilePath)
		{
			return new SiteImportSettings(null)
			{
				SourceFilePath = sourceFilePath,
				TemporaryFilesPath = Path.GetTempPath(),
				WebsitePath = @"\",
				ImportType = importType
			};
		}

		public bool ShouldAuditPackages
		{
			get { return importType != ImportTypeEnum.All; }
		}
	}
}