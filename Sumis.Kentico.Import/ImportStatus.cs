﻿namespace Sumis.Kentico.Import
{
    public enum ImportStatus
    {
        Finished,
        Error,
        Warning
    }
}