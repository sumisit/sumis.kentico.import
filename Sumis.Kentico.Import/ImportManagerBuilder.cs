﻿using CMS.CMSImportExport;
using Sumis.Kentico.Import.PackageFiles;
using System;
using System.Collections.Generic;

namespace Sumis.Kentico.Import
{
	public class ImportManagerBuilder
	{
		private List<IImportPackageProvider> importPackageProviders = new List<IImportPackageProvider>();

		public ImportManager Build()
		{
			if (importPackageProviders.Count == 0)
				throw new NotSupportedException("Choose at least one package provider using the With methods");

			return new ImportManager(importPackageProviders);
		}

		public ImportManagerBuilder WithRunOnlyNewPackages(string folder, bool recursive = false)
		{
			if (string.IsNullOrEmpty(folder))
				throw new ArgumentNullException("folder");

			importPackageProviders.Add(new ImportTypePackageProvider(
				new FolderPackageProvider(folder, recursive),
				ImportTypeEnum.New)
				);

			return this;
		}

		public ImportManagerBuilder WithRunAlwaysPackages(string folder, bool recursive = false)
		{
			if (string.IsNullOrEmpty(folder))
				throw new ArgumentNullException("folder");

			importPackageProviders.Add(new ImportTypePackageProvider(
				new FolderPackageProvider(folder, recursive),
				ImportTypeEnum.All)
				);

			return this;
		}
	}
}