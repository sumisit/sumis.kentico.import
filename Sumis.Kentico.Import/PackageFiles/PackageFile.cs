﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sumis.Kentico.Import.PackageFiles
{
	public class PackageFile
	{
		public string FilePath { get; private set; }

		public PackageFileMetadata Options { get; set; }

		public PackageFile(string filePath)
		{
			if (filePath == null)
				throw new ArgumentNullException("filePath");

			FilePath = filePath;
		}
	}
}
