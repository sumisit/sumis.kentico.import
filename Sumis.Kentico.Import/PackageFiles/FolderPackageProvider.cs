﻿using Sumis.Kentico.Import.PackageFiles.ImportOptionsSerialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Sumis.Kentico.Import.PackageFiles
{
	public class FolderPackageProvider : IPackageFileProvider
	{
		private readonly string folder;
		private readonly bool recursive;

		public FolderPackageProvider(string folder, bool recursive = false)
		{
			this.folder = folder;
			this.recursive = recursive;
		}

		public IEnumerable<PackageFile> GetPackageFiles()
		{
			if (!Directory.Exists(folder))
				yield break;

			var packageFilePaths = Directory.GetFiles(folder, "*.zip", recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);

			if (packageFilePaths == null)
				yield break;

			foreach (var packageFilePath in packageFilePaths)
			{
				PackageFile packageFile = new PackageFile(packageFilePath);
				var metadataFile = Path.ChangeExtension(packageFilePath, "xml");

				if (File.Exists(metadataFile))
				{
					try
					{
						ImportOptions options = null;

						XmlSerializer serializer = new XmlSerializer(typeof(ImportOptions));

						using (var xmlReader = XmlReader.Create(metadataFile))
						{
							options = (ImportOptions)serializer.Deserialize(xmlReader);
							if (options.Sites != null)
							{
								packageFile.Options = new PackageFileMetadata()
								{
									SiteCodes = options.Sites.Select(c => c.Code.Trim()).ToList()
								};
							}
						}
					}
					catch (Exception e)
					{
						throw new Exception(string.Format("Failed to parse import options file {0} for package {1}", metadataFile, packageFilePath), e);
					}
				}
				yield return packageFile;
			}
		}
	}
}
