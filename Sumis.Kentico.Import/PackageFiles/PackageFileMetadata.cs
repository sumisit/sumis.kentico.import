﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sumis.Kentico.Import.PackageFiles
{
	public class PackageFileMetadata
	{
		/// <summary>
		/// The site codes for which the package should run
		/// </summary>
		public IEnumerable<string> SiteCodes { get; set; }

		public PackageFileMetadata()
		{
			SiteCodes = new List<string>();
		}
	}
}
