﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sumis.Kentico.Import.PackageFiles
{
	public interface IPackageFileProvider
	{
		IEnumerable<PackageFile> GetPackageFiles();
	}
}
