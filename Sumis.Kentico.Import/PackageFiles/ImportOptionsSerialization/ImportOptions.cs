﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Sumis.Kentico.Import.PackageFiles.ImportOptionsSerialization
{
	[XmlRoot("importOptions"), XmlType("ImportOptions")]
	public class ImportOptions
	{
		[XmlArray(ElementName = "sites")]
		[XmlArrayItem(ElementName = "site")]
		public List<Site> Sites { get; set; }
	}


	public class Site
	{
		[XmlElement(ElementName = "code")]
		public string Code { get; set; }
	}
}
