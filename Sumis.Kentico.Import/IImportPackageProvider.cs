﻿using CMS.CMSImportExport;
using System.Collections.Generic;

namespace Sumis.Kentico.Import
{
	public interface IImportPackageProvider
	{
		/// <summary>
		/// Indicates whether packages should be auditted when ran.
		/// If so, it will only run packages which are not auditted before
		/// </summary>
		bool ShouldAuditPackages { get; }

		IEnumerable<SiteImportSettings> GetImportPackages();
	}
}