﻿using System;
using System.Collections.Generic;
using System.IO;
using CMS.CMSHelper;
using CMS.CMSImportExport;
using System.Data.SqlClient;
using System.Configuration;
using log4net;
using Sumis.Kentico.Import.Audit;
using System.Data;
using System.Linq;

namespace Sumis.Kentico.Import
{
	public class ImportManager
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(ImportManager));

		private readonly IImportPackageProvider importPackageProvider;
		private readonly IEnumerable<IImportPackageProvider> importPackageProviders;
		private readonly string connectionString;


		public ImportManager(IEnumerable<IImportPackageProvider> importPackageProviders)
		{
			if (importPackageProviders == null)
				throw new ArgumentNullException("importPackageProviders");

			this.importPackageProviders = importPackageProviders;
			this.connectionString = ConfigurationManager.ConnectionStrings["CMSConnectionString"].ConnectionString;
		}


		public IEnumerable<ImportResult> Import()
		{
			CMSContext.Init();

			//if any of the package providers demands auditting, ensure the audit table
			if (importPackageProviders.Any(packageProvider => packageProvider.ShouldAuditPackages))
				EnsurePackageAuditTable();


			foreach (var packageProvider in importPackageProviders)
			{
				var importPackages = packageProvider.GetImportPackages();

				if (importPackages == null)
				{
					yield break;
				}

				var alreadyImportedPackages = new List<ImportPackageAudit>();

				if (packageProvider.ShouldAuditPackages)
				{
					alreadyImportedPackages = GetExecutedPackages();
				}

				foreach (var importPackage in importPackages)
				{
					var packageFilename = Path.GetFileName(importPackage.SourceFilePath);

					Log.InfoFormat("Found package {0}...", packageFilename);

					if (alreadyImportedPackages.FirstOrDefault(c =>
							packageFilename == c.Filename
							&& (c.SiteID ?? 0) == importPackage.SiteId
						) != null
					)
					{
						Log.InfoFormat("Skipping package {0} because it already has been run...", packageFilename);
						continue;
					}

					Log.InfoFormat("Executing package {0}...", packageFilename);

					ImportProvider.CreateTemporaryFiles(importPackage);

					var importManager = new CMS.CMSImportExport.ImportManager(importPackage);
					importManager.Import(null);
					var importResult = new ImportResult(importPackage, importPackage.SourceFilePath);

					if (packageProvider.ShouldAuditPackages && importResult.Status == ImportStatus.Finished)
					{
						AuditPackage(importPackage);
						Log.InfoFormat("Saved package {0} as executed...", packageFilename);
					}

					yield return importResult;
				}
			}
		}

		private void EnsurePackageAuditTable()
		{
			using (var sqlConnection = new SqlConnection(connectionString))
			{
				sqlConnection.Open();
				using (var checkTableCmd = sqlConnection.CreateCommand())
				{
					checkTableCmd.CommandText = @"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KenticoPackageImportAudit]') AND type in (N'U'))
			BEGIN
			CREATE TABLE KenticoPackageImportAudit(
				ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_KenticoPackageImportAudit PRIMARY KEY CLUSTERED,
				ScriptFileName nvarchar(255) NOT NULL,
				RunTimestamp datetimeoffset(0) NOT NULL,
				SiteID int
			)END";
					Log.Info("Creating KenticoPackageImportAudit table if not exists...");

					checkTableCmd.ExecuteNonQuery();
				}
			}
		}

		private List<ImportPackageAudit> GetExecutedPackages()
		{
			using (var connection = new SqlConnection(connectionString))
			{
				connection.Open();
				using (var selectCommand = connection.CreateCommand())
				{
					selectCommand.CommandText = @"select ScriptFileName, SiteID from KenticoPackageImportAudit";
					List<ImportPackageAudit> importedPackages = new List<ImportPackageAudit>();

					using (var reader = selectCommand.ExecuteReader())
					{
						while (reader.Read())
						{
							importedPackages.Add(
								new ImportPackageAudit
								{
									Filename = reader.GetString(0),
									SiteID = reader.GetInt32(1)
								});
						}
					}

					return importedPackages;
				}
			}
		}

		/// <summary>
		/// Store the package 
		/// </summary>
		/// <param name="importPackage"></param>
		private void AuditPackage(SiteImportSettings importPackage)
		{
			using (var connection = new SqlConnection(connectionString))
			{
				connection.Open();
				using (var auditPackageCommand = connection.CreateCommand())
				{
					auditPackageCommand.CommandText = @"INSERT INTO KenticoPackageImportAudit (ScriptFileName, RunTimestamp, SiteID) values (@ScriptFilename, @RunTimestamp, @SiteID)";
					auditPackageCommand.Parameters.Add("ScriptFileName", SqlDbType.NVarChar, 255).Value = Path.GetFileName(importPackage.SourceFilePath);
					auditPackageCommand.Parameters.Add("RunTimestamp", SqlDbType.DateTimeOffset, 4).Value = DateTimeOffset.Now;
					auditPackageCommand.Parameters.Add("SiteID", SqlDbType.Int).Value = importPackage.SiteId;

					auditPackageCommand.ExecuteNonQuery();
				}
			}
		}
	}
}