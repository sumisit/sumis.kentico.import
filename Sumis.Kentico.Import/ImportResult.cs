﻿using System;
using System.Collections.Generic;
using System.Linq;
using CMS.CMSImportExport;

namespace Sumis.Kentico.Import
{
    public class ImportResult
    {
        private readonly string[] logParts;
        private readonly SiteImportSettings settings;
        private readonly string importPackageFilePath;

        internal ImportResult(SiteImportSettings settings, string importPackageFilePath)
        {
            if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }
            this.settings = settings;
            this.importPackageFilePath = importPackageFilePath;
            logParts = settings.ProgressLog.Split(new[] {"<#>"}, StringSplitOptions.None);
        }

        public string ImportPackageFilePath
        {
            get { return importPackageFilePath; }
        }

        public ImportStatus Status
        {
            get
            {
                if (settings.IsError())
                {
                    return ImportStatus.Error;
                }
                if (settings.IsWarning())
                {
                    return ImportStatus.Warning;
                }
                return ImportStatus.Finished;
            }
        }

        public IEnumerable<string> Messages
        {
            get { return GetLogLines(1); }
        }

        public IEnumerable<string> Errors
        {
            get { return GetLogLines(2); }
        }

        public IEnumerable<string> Warnings
        {
            get { return GetLogLines(3); }
        }

        private IEnumerable<string> GetLogLines(int index)
        {
            if (logParts.Length > index)
            {
                return ToLines(logParts[index]);
            }
            return Enumerable.Empty<string>();
        }

        private static IEnumerable<string> ToLines(string log)
        {
            return log.Split(new[] {"<br />"}, StringSplitOptions.RemoveEmptyEntries).Reverse();
        }
    }
}