﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sumis.Kentico.Import.Audit
{
	public class ImportPackageAudit
	{
		public string Filename { get; set; }

		public int? SiteID { get; set; }
	}
}
